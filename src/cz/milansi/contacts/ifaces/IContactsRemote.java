package cz.milansi.contacts.ifaces;

import java.util.List;

import javax.ejb.Remote;
import cz.milansi.contacts.dto.*;

@Remote
public interface IContactsRemote {
	
	/**
	 * Testovaci metoda pro zjisteni funkcnosti.
	 * @param instr vstupni retezec
	 * @return "PONG" + vstupni retezec
	 */
	public String ping (String instr);
	
	/**
	 * Vrati seznam osob z tabulky person
	 */
	public List<DTOPerson> getPersons();
	
	/**
	 * Vrati detail jedne osoby dle id
	 */
	public DTOPerson getPersonDetail(int id);
	
	/**
	 * prida novou osobu a vrati jeji id
	 */
	public int addPerson (String jmeno, String prijmeni);


	/** 
	 * vraci osobu vcetne nactenych kontaktu
	 * @param id id
	 */
	public DTOPerson getPersonWithContacts(int id); 



}
