package cz.milansi.contacts.dto;

/**
 * Trida reprezentuje prenosovy objekt Contact.
 * @author msiebenb
 *
 */
public class DTOPersonContact implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DTOPersonContact() {
	}
	
	// id
	private int id = 0;
	// vazba na osobu
	private DTOPerson person = null;
	// typ kontaktu - 1 tlf, 2 email
	private int typ = 0;
	// hodnota kontaktu 
	private String hodnota = null;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the person
	 */
	public DTOPerson getPerson() {
		return person;
	}
	/**
	 * @param person the person to set
	 */
	public void setPerson(DTOPerson person) {
		this.person = person;
	}
	/** 
	 * @return the typ
	 */
	public int getTyp() {
		return typ;
	}
	/**
	 * @param typ the typ to set
	 */
	public void setTyp(int typ) {
		this.typ = typ;
	}
	/**
	 * @return the hodnota
	 */
	public String getHodnota() {
		return hodnota;
	}
	/**
	 * @param hodnota the hodnota to set
	 */
	public void setHodnota(String hodnota) {
		this.hodnota = hodnota;
	}
	
	/**
	 * Prevod do Stringu
	 */
	public String toString() {
		return "DTOPersonContact (person = " + person + ", typ = " + typ + ", hodnota = " + hodnota + ")";
	}

}
