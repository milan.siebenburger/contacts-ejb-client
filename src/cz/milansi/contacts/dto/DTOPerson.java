package cz.milansi.contacts.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Trida reprezentuje prenosovy objekt Person.
 * 
 * @author msiebenb
 *
 */
public class DTOPerson implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DTOPerson() {
	}
	
	// person.id
	private int id;
	// person.jmeno
	private String jmeno;
	// person.prijmeni
	private String prijmeni;
	// person.datvzn
	private Date datvzn;
	// kontakty
	private List<DTOPersonContact> kontakty;
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	/*ID nebudeme nastavovat 
	 * public void setId(int id) {
	
		this.id = id;
	}/*
	/**
	 * @return the jmeno
	 */
	public String getJmeno() {
		return jmeno;
	}
	/**
	 * @param jmeno the jmeno to set
	 */
	public void setJmeno(String jmeno) {
		this.jmeno = jmeno;
	}
	/**
	 * @return the prijmeni
	 */
	public String getPrijmeni() {
		return prijmeni;
	}
	/**
	 * @param prijmeni the prijmeni to set
	 */
	public void setPrijmeni(String prijmeni) {
		this.prijmeni = prijmeni;
	}
	/**
	 * @return the datvzn
	 */
	public Date getDatvzn() {
		return datvzn;
	}
	/**
	 * @param datvzn the datvzn to set
	 */
	public void setDatvzn(Date datvzn) {
		this.datvzn = datvzn;
	}
	/**
	 * Vraci kontakty k tomuto cloveku
	 * @return
	 */
	public List<DTOPersonContact> getKontakty() {		
		return kontakty;
	}
	
	public Optional<List<DTOPersonContact>> getKontaktySafe() {
		if (kontakty != null) {
	    	return Optional.of(kontakty);
		} else {
			return Optional.empty();
		}
	}
	
	/**
	 * Nastavi kontakty.
	 * @param kontakty
	 */
	public void setKontakty(List<DTOPersonContact> kontakty) {
		this.kontakty = kontakty;
	}
	
	/** 
	 * prida novy kontakt.
	 * @param pc
	 */
	public void add(DTOPersonContact pc) {
		if (kontakty == null) {
			kontakty = new ArrayList<DTOPersonContact>();
		}
		kontakty.add(pc);
	}
	
	/**
	 * Prevod do Stringu..
	 */
	public String toString() {
		return "DTOPerson (prijmeni = " + prijmeni + ", jmeno = " + jmeno;
	}
	
	

}
