package cz.milansi.contacts.client;

import java.util.List;
import java.util.Optional;

import javax.naming.Context;
import javax.naming.NamingException;

import cz.milansi.contacts.dto.DTOPerson;
import cz.milansi.contacts.dto.DTOPersonContact;
import cz.milansi.contacts.ifaces.IContactsRemote;

public class TestClient {

	public TestClient() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		IContactsRemote bean = doLookup();
		System.out.println ("BEAN je : " + bean);
		bean.ping("Test ping");
		System.out.println ("bean.ping " + bean.ping ("Test ping"));
		
		System.out.println (bean.getPersonDetail(1).getJmeno() + " " + bean.getPersonDetail(1).getPrijmeni());
		System.out.println (bean.getPersonDetail(1).getDatvzn());
		
		
		List<DTOPerson> list = bean.getPersons();
		
		for (DTOPerson p : list) {
			System.out.println ("----------------------------------");
			System.out.println (p);

			
			Optional<List<DTOPersonContact>> kontakty = p.getKontaktySafe();
			
			kontakty.ifPresent(kont -> kont.forEach(System.out::println));
			if (!kontakty.isPresent()) {
				System.out.println ("nema kontakty");
			}
		}
		
		
		
		
	}
		
	private static IContactsRemote doLookup() {
       // final String BEAN_NAME = "java:global/contacts-ejb/ContactsBean";
        //final String BEAN_NAME = "ejb:contacts-ear-0.0.1-SNAPSHOT/contacts-ejb-0.0.1-SNAPSHOT/ContactsBean!cz.milansi.contacts.ifaces.IContactsRemote";
        final String BEAN_NAME = "ejb:contacts-ear-0.0.1-SNAPSHOT/contacts-project-contacts-ejb-0.0.1-SNAPSHOT/ContactsBean!cz.milansi.contacts.ifaces.IContactsRemote";
        Context context = null;
        IContactsRemote result = null;

        System.out.println (BEAN_NAME); 
        try {
           context = ClientUtility.getInitialContext();
           result = (IContactsRemote) context.lookup(BEAN_NAME);
        } catch (NamingException e) {
                e.printStackTrace();
        }


        return result;

	}

}
